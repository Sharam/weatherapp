//
//  ServiceTests.swift
//  Tests iOS
//
//  Created by Ramdhas on 04/02/22.
//

import XCTest
@testable import Weather

// Test MetaWeather API
class ServicesTests: XCTestCase {
    
    let london = Location(woeid: 44418, title: "London", lattLong: "51.506321, -0.12714")
    
    func testLocations() async throws {
        // by query
        let fetchedLondon = try await LocationService.fetchLocation(byText: "london").first
        XCTAssertEqual(fetchedLondon?.woeid, london.woeid)
    }

    func testWeather() async throws {
        do {
            let locationWeather = try await WeatherService.fetchWeather(location: london)
            XCTAssertGreaterThan(locationWeather.count, 0)
            XCTAssert(locationWeather[0].applicableDate.isSameDay(of: Date()))
        } catch {
            XCTFail("Expected fetch weather for location, but failed \(error)")
        }
    }
    
    func testIntegration() async throws {
        do {
            let londonSearch = try await LocationService.fetchLocation(byText: "london")
            XCTAssertGreaterThan(londonSearch.count, 0)
            let londonWeather = try await WeatherService.fetchWeather(location: londonSearch[0])
            XCTAssertGreaterThan(londonWeather.count, 0)
            let berlinSearch = try await LocationService.fetchLocation(byText: "berlin")
            XCTAssertGreaterThan(berlinSearch.count, 0)
        } catch {
            XCTFail("Could not fetch this location")
        }
    }
    
}

extension Date {
    func isSameDay(of date: Date) -> Bool {
        let diff = Calendar.current.dateComponents([.day], from: self, to: date)
        return diff.day == 0
    }
}
