//
//  ViewStatus.swift
//  WeatherApp
//
//  Created by Ramdhas on 04/02/22.
//

import Foundation

enum ViewStatus: Equatable {
    case idle
    case fetchingLocations
    case locationsLoaded([Location])
    case fetchingWeather
    case weathersLoaded([Weather], location: Location)
    case fetchingCurrentLocation
    case empty
    case error(message: String)
}
