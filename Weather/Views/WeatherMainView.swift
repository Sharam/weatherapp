//
//  WeatherMainView.swift
//  Shared
//
//  Created by Ramdhas on 04/02/22.
//

import SwiftUI
import CoreLocationUI

struct WeatherMainView: View {
    
    @StateObject var viewModel = WeatherViewModel()
    
    @State private var searchQuery: String = ""
    
    var searchPlacement: SearchFieldPlacement {
        SearchFieldPlacement.navigationBarDrawer(displayMode: .always)
    }
    
    var filteredSuggestions: [String] {
        viewModel.searchSuggestions.filter {
            $0.lowercased().hasPrefix(searchQuery.lowercased())
        }
    }
    
    var body: some View {
        NavigationView {
            ZStack {
                BackgroundView()
                WeatherResultsView(
                    viewStatus: viewModel.viewStatus,
                    onSelectLocation: { location in
                        searchQuery = location.title
                        Task { await viewModel.fetchWeather(for: location) }
                    },
                    onCancelSearch: {
                        guard let location = viewModel.currentLocation else { return }
                        Task { await viewModel.fetchWeather(for: location) }
                    }
                )
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .navigationTitle("WeatherApp")
            .navigationBarTitleDisplayMode(.inline)

            .searchable(
                text: $searchQuery,
                placement: searchPlacement, 
                prompt: "Search location"
            ) {
                switch viewModel.viewStatus {
                case .idle, .weathersLoaded, .empty, .error:
                    searchSuggestionsView
                default:
                    EmptyView()
                }
            }
        }
        .navigationViewStyle(.stack)
        .preferredColorScheme(.dark)
        .task {
            await viewModel.fetchLocationThenWeather(by: viewModel.initialLocation)
        }
        .onSubmit(of: .search) {
            Task { await viewModel.fetchLocations(by: .query(searchQuery)) }
        }
        //.onReceive
    }
    
    var searchSuggestionsView: some View {
        VStack(alignment: .leading, spacing: 14) {
            if searchQuery.count == 0 {
                Text("Cities")
                    .font(.title3)
                    .fontWeight(.bold)
                ForEach(filteredSuggestions, id: \.self) { text in
                    Text(text)
                        .contentShape(Rectangle())
                        .searchCompletion(text)
                        .onTapGesture {
                            searchQuery = text
                            Task { await viewModel.fetchLocationThenWeather(by: text) }
                        }
                }
                Spacer()
                    .frame(height: 10)
            } else {
                Text("Type a location and enter to search")
            }
            Spacer()
        }
        
    }
    
}

struct WeatherView_Previews: PreviewProvider {
    static var previews: some View {
        WeatherMainView()
    }
}
