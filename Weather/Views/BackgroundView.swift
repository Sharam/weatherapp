//
//  BackgroundView.swift
//  WeatherApp
//
//  Created by Ramdhas on 04/02/22.
//

import SwiftUI

struct BackgroundView: View {
    var body: some View {
        let colorScheme = [Color(red: 53/255, green: 92/255, blue: 125/255),
                           Color(red: 108/255, green: 91/255, blue: 123/255),
                           Color(red: 192/255, green: 108/255, blue: 132/255)]
        
        let gradient = Gradient(colors: colorScheme)
        let linearGradient = LinearGradient(gradient: gradient, startPoint: .top, endPoint: .bottom)
        
        let background = Rectangle()
            .fill(linearGradient)
            .blur(radius: 200, opaque: true)
            .edgesIgnoringSafeArea(.all)
        
        return background
    }
}
