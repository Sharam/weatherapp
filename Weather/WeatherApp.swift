//
//  WeatherApp.swift
//  Weather
//
//  Created by Ramdhas on 04/02/22.
//

import SwiftUI

@main
struct WeatherApp: App {
    var body: some Scene {
        WindowGroup {
            WeatherMainView()
        }
    }
}
